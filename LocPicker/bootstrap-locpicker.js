!function($) {
    var LocPicker = function(element, options) {
        this.options = $.extend($.fn.locpicker.defaults, options);
        
        this.$element = $(element);
        this.$element.on('click', $.proxy(this.show, this));
        
        init.call(this);
    };
    
    LocPicker.prototype = {
        constructor: LocPicker,
        
        update: function(data) {
            this.request && this.request.abort();
            if (typeof data === 'string') {
                this.options.url = data;
                this.request = $.getJSON(data, $.proxy(this.update, this));
                return;
            }

            // 如果定义了数据加工方法，则使用该方法加工数据为符合规则的形式
            this.options.data = data;
            var dataHandler = this.options.dataHandler;
            this.data = dataHandler ? dataHandler.call(this, data) : this.options.data;

            fill.call(this, this.data);
        },
        select: function(evt) {
            var location = $(evt.target).data();

            this.$element.val(location.name);
            this.$element.next('input[type=hidden]').val(location.code);
            this.options.callback.call(this.$element, location);
            this.hide();
        },
        show: function(e) {
            e.stopPropagation();
            e.preventDefault();
            
            var pos = $.extend({}, this.$element.offset(), {
                height: this.$element[0].offsetHeight
            });
            this.$widget.css({
                top: pos.top + pos.height,
                left: pos.left
            });

            this.$widget.show();
            $('html').on('click.locpicker.data-api', $.proxy(this.hide, this));
        },
        showDetail: function(evt) {
            var $target = $(evt.target);
            var $detail = $target.next('.detail');
            var top = $target.position().top;
            var left = $target.position().left + $target.width();
            $detail.css({ top: top, left: left });
            $detail.show();
        },
        hideDetail: function(evt) {
            var $target = $(evt.target);
            var $detail = $target.next('.detail');
            $detail.hide();
        },
        hide: function(e) {
            this.$widget.hide();
            $('html').off('click.locpicker.data-api', $.proxy(this.hide, this));
        }
    };
   
    // == LocPicker Private Methods ==

    // 使用提供的参数初始化控件
    function init() {
        var $this = this;
        this.$widget = $(this.options.tpls.dropdown).appendTo('body');
        this.$widget.on('click', '[data-action=select]', $.proxy(this.select, this));
        this.$widget.on('mouseover', '[data-toggle=hover]', $.proxy(this.showDetail, this));
        this.$widget.on('mouseout', '[data-toggle=hover]', $.proxy(this.hideDetail, this));
        
        // 根据提供的数据源，载入数据，如果直接提供了数据，则不需要通过ajax载入数据
        this.update.call(this, this.options.data || this.options.url);
    }
    
    // 将数据填充到控件中
    function fill(data) {
        var $this = this;
        var html = $.map(data, function(district) {
            return fmt($this.options.tpls.district, district)
                +  $.map(district.locations, function(location) {
                        return fmt($this.options.tpls.location, location);
                   }).join('');
        }).join('');

        // 清空控件内容
        $(html).appendTo($this.$widget.find('.loc-list').empty());
    }

    // == Global Private Methods ==

    // 格式化字符串
    // 
    // 用法：
    // 
    // var s1 = 'I like %{1} and %{2}!';
    // console.log('source: ' + s1);
    // console.log('target: ' + fmt(s1, 'ask', 'learn'));
    // 
    // var s2 = "%{name} is %{age} years old, his son's name is %{sons[0].name}";
    // console.log('source: ' + s2);
    // console.log('target: ' + fmt(s2, { name: 'Lao Ming', age: 32, sons: [{ name: 'Xiao Ming', age: 12}]}));
    function fmt() {
        var args = arguments;
        return args[0].replace(/%\{(.*?)}/g, function(match, prop) {
            return function(obj, props) {
                var prop = /\d+/.test(props[0]) ? parseInt(props[0]) : props[0];
                if (props.length > 1) {
                    return arguments.callee(obj[prop], props.slice(1));
                } else {
                    return obj[prop];
                }
            }(typeof args[1] === 'object' ? args[1] : args, prop.split(/\.|\[|\]\[|\]\./));
        });
    }
    
    // 门店选择控件jQuery接口定义
    //
    // 参数定义：
    // =====================================
    // url         {string}   通过ajax请求返回数据
    // data        {object}   直接使用提供的门店原始数据 (如果提供了data，则忽略url)
    // dataHandler {function} 自定义的数据加工方法 参数：data
    // callback    {function} 当选择了一个门店后执行的架设方法 参数：location
    $.fn.locpicker = function(option) {
        var args = arguments;
        return this.each(function () {
            var $this = $(this);
            var locpicker = $this.data('locpicker');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);
            if (!locpicker) $this.data('locpicker', (locpicker = new LocPicker(this, options)));
            if (typeof option === 'string') {
                locpicker[option].apply(locpicker, Array.prototype.slice.call(args, 1));
            }
        });
    };

    // 默认参数定义
    $.fn.locpicker.defaults = {
        tpls: {
            dropdown: '<div class="bootstrap-locpicker hide">'
                    + '<dl class="loc-list"></dl>'
                    + '<div class="detail hide"></div>' 
                    + '</div>',
            district: '<dt>%{name}</dt>',
            location: '<dd>'
                        + '<a href="javascript:;" data-action="select" data-toggle="hover" '
                            + 'data-name="%{name}" data-code="%{code}">%{name}</a>'
                        + '<div class="detail hide">'
                            + '<h4>%{name}</h4>'
                            + '<dl>'
                                + '<dt>门店地址：</dt><dd>%{address}</dd>'
                                + '<dt>门店电话：</dt><dd>%{phone}</dd>'
                                + '<dt>营业时间：</dt>'
                                + '<dd>'
                                    + '<div>%{worktime[0]}</div>'
                                    + '<div>%{worktime[1]}</div>'
                                    + '<div>%{worktime[2]}</div>'
                                    + '<div>%{worktime[3]}</div>'
                                    + '<div>%{worktime[4]}</div>'
                                    + '<div>%{worktime[5]}</div>'
                                    + '<div>%{worktime[6]}</div>'
                                + '</dd>'
                            + '</dl>'
                        + '</div>'
                    + '</dd>',
        }         
    };

    $.fn.locpicker.Constructor = LocPicker;
}(window.jQuery);
