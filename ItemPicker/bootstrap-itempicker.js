!function($) {
    var ItemPicker = function(element, options) {
        this.options = $.extend($.fn.itempicker.defaults, options);
        
        this.$element = $(element);
        this.$element.on('click', $.proxy(this.show, this));
        this.$element.on('focus', $.proxy(this.show, this));
        this.$element.on('blur',  $.proxy(this.hide, this));
        init.call(this);
    };
    
    ItemPicker.prototype = {
        constructor: ItemPicker,
        
        update: function(data) {
            this.request && this.request.abort();
            if (typeof data === 'string') {
                this.options.url = data;
                this.request = $.getJSON(data, this.options.params, $.proxy(this.update, this));
                return;
            }

            // 如果定义了数据加工方法，则使用该方法加工数据为符合规则的形式
            this.options.data = data;
            var dataHandler = this.options.dataHandler;
            this.data = dataHandler ? dataHandler.call(this, data) : this.options.data;

            fill.call(this, this.data);
        },
        select: function(evt) {
            var item = $(evt.target).data();

            this.$element.val(item.name);
            this.$element.next('input[type=hidden]').val(item.code);
            this.options.callback && this.options.callback.call(this.$element, item);
            this.ignoreBlur = false;
            this.hide();
        },
        show: function(evt) {
            evt.stopPropagation();
            // e.preventDefault();
            
            var pos = $.extend({}, this.$element.offset(), {
                height: this.$element[0].offsetHeight
            });
            this.$widget.css({
                top: pos.top + pos.height,
                left: pos.left
            });

            this.$widget.show();
            $('html').on('click.itempicker.data-api', $.proxy(this.hide, this));
        },
        hide: function(evt) {
            if (this.ignoreBlur) {
                this.ignoreBlur = false;
                return;
            }
            this.$widget.hide();
            $('html').off('click.itempicker.data-api', $.proxy(this.hide, this));
        },
        mousedown: function(evt) {
            this.ignoreBlur = true;
        }
    };
   
    // == ItemPicker Private Methods ==

    // 使用提供的参数初始化控件
    function init() {
        var $this = this;
        this.$widget = $(this.options.tpls.dropdown).appendTo('body');
        
        // 保存下拉层的默认宽度
        if (this.defaultWidth) this.defaultWidth = $this.$widget.width();
        
        // 动态设置下拉层的宽度
        var width = 'default';
        if ($this.options.width == 'input') {
            width = this.$element.innerWidth();
        } else if (this.options.width == 'default') {
            width = this.defaultWidth + 'px';
        } else {
            width = this.options.width;
        }
        this.$widget.css('width', width);

        this.$widget.on('mousedown', $.proxy(this.mousedown, this));
        this.$widget.on('click', '[data-action=select]', $.proxy(this.select, this));
        
        // 根据提供的数据源，载入数据，如果直接提供了数据，则不需要通过ajax载入数据
        this.update.call(this, this.options.data || this.options.url);
    }
    
    // 将数据填充到控件中
    function fill(data) {
        var $this = this;
        var html = $.map(data, function(district) {
            return fmt($this.options.tpls.item, district);
        }).join('');

        // 清空控件内容
        $(html).appendTo($this.$widget.find('.item-list').empty());

        // 如果设置了默认选中，则在填充数据后选中设定的项
        if (/\d+/.test(this.options.select)) {
            var selector = fmt('li:eq(%{1}) a', this.options.select);
            this.$widget.find(selector).trigger('click');
        }

    }

    // == Global Private Methods ==

    // 格式化字符串
    // 
    // 用法：
    // 
    // var s1 = 'I like %{1} and %{2}!';
    // console.log('source: ' + s1);
    // console.log('target: ' + fmt(s1, 'ask', 'learn'));
    // 
    // var s2 = "%{name} is %{age} years old, his son's name is %{sons[0].name}";
    // console.log('source: ' + s2);
    // console.log('target: ' + fmt(s2, { name: 'Lao Ming', age: 32, sons: [{ name: 'Xiao Ming', age: 12}]}));
    function fmt() {
        var args = arguments;
        return args[0].replace(/%\{(.*?)}/g, function(match, prop) {
            return function(obj, props) {
                var prop = /\d+/.test(props[0]) ? parseInt(props[0]) : props[0];
                if (props.length > 1) {
                    return arguments.callee(obj[prop], props.slice(1));
                } else {
                    return obj[prop];
                }
            }(typeof args[1] === 'object' ? args[1] : args, prop.split(/\.|\[|\]\[|\]\./));
        });
    }
    
    // 门店选择控件jQuery接口定义
    //
    // 参数定义：
    // =====================================
    // url         {string}   通过ajax请求返回数据
    // params      {object}   如果选择url作为数据源，params为提交的参数
    // data        {object}   直接使用提供的门店原始数据 (如果提供了data，则忽略url)
    // dataHandler {function} 自定义的数据加工方法 参数：data
    // callback    {function} 当选择了一个门店后执行的架设方法 参数：location
    // select      {integer}  默认选择的项的索引
    // width       {string}   下拉层的宽度
    //                        `input`   与输入框等宽 (width+padding, 不包括 border )   
    //                        `default` 默认的宽度 (220px)   
    //                         width    css width 属性值的写法   
    $.fn.itempicker = function(option) {
        var args = arguments;
        return this.each(function () {
            var $this = $(this);
            var itempicker = $this.data('itempicker');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);
            if (!itempicker) $this.data('itempicker', (itempicker = new ItemPicker(this, options)));
            if (typeof option === 'string') {
                itempicker[option].apply(itempicker, Array.prototype.slice.call(args, 1));
            }
        });
    };

    // 默认参数定义
    $.fn.itempicker.defaults = {
        select: 0,
        width: 'input',
        params: {},
        tpls: {
            dropdown: '\
                <div class="bootstrap-itempicker hide">\
                    <ul class="item-list unstyled"></ul>',
            item: '\
                <li class="item">\
                    <a href="javascript:;" data-action="select" data-code="%{code}" data-name="%{name}">%{name}</a>\
                </li>',
        } 
    };

    $.fn.itempicker.Constructor = ItemPicker;
}(window.jQuery);
