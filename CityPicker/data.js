{
    "all_cities": [
        {"name": "好长名字的市", "pinyin": "a0", "code": "a-0"},
        {"name": "城市a1", "pinyin": "a1", "code": "a-1"},
        {"name": "城市a2", "pinyin": "a2", "code": "a-2"},
        {"name": "城市a3", "pinyin": "a3", "code": "a-3"},
        {"name": "城市a4", "pinyin": "a4", "code": "a-4"},
        {"name": "城市a5", "pinyin": "a5", "code": "a-5"},
        {"name": "城市a6", "pinyin": "a6", "code": "a-6"},
        {"name": "城市a7", "pinyin": "a7", "code": "a-7"},
        {"name": "呼和浩特市", "pinyin": "a8", "code": "a-8"},
        {"name": "城市a9", "pinyin": "a9", "code": "a-9"},
        {"name": "城市a10", "pinyin": "a10", "code": "a-10"},
        {"name": "城市a11", "pinyin": "a11", "code": "a-11"},
        {"name": "城市a12", "pinyin": "a12", "code": "a-12"},
        {"name": "城市a13", "pinyin": "a13", "code": "a-13"},
        {"name": "城市a14", "pinyin": "a14", "code": "a-14"},
        {"name": "城市b0", "pinyin": "b0", "code": "b-0"},
        {"name": "城市b1", "pinyin": "b1", "code": "b-1"},
        {"name": "城市b2", "pinyin": "b2", "code": "b-2"},
        {"name": "城市b3", "pinyin": "b3", "code": "b-3"},
        {"name": "城市b4", "pinyin": "b4", "code": "b-4"},
        {"name": "城市b5", "pinyin": "b5", "code": "b-5"},
        {"name": "城市b6", "pinyin": "b6", "code": "b-6"},
        {"name": "城市b7", "pinyin": "b7", "code": "b-7"},
        {"name": "城市b8", "pinyin": "b8", "code": "b-8"},
        {"name": "城市b9", "pinyin": "b9", "code": "b-9"},
        {"name": "城市b10", "pinyin": "b10", "code": "b-10"},
        {"name": "城市b11", "pinyin": "b11", "code": "b-11"},
        {"name": "城市b12", "pinyin": "b12", "code": "b-12"},
        {"name": "城市b13", "pinyin": "b13", "code": "b-13"},
        {"name": "城市b14", "pinyin": "b14", "code": "b-14"},
        {"name": "城市c0", "pinyin": "c0", "code": "c-0"},
        {"name": "城市c1", "pinyin": "c1", "code": "c-1"},
        {"name": "城市c2", "pinyin": "c2", "code": "c-2"},
        {"name": "城市c3", "pinyin": "c3", "code": "c-3"},
        {"name": "城市c4", "pinyin": "c4", "code": "c-4"},
        {"name": "城市c5", "pinyin": "c5", "code": "c-5"},
        {"name": "城市c6", "pinyin": "c6", "code": "c-6"},
        {"name": "城市c7", "pinyin": "c7", "code": "c-7"},
        {"name": "城市c8", "pinyin": "c8", "code": "c-8"},
        {"name": "城市c9", "pinyin": "c9", "code": "c-9"},
        {"name": "城市c10", "pinyin": "c10", "code": "c-10"},
        {"name": "城市c11", "pinyin": "c11", "code": "c-11"},
        {"name": "城市c12", "pinyin": "c12", "code": "c-12"},
        {"name": "城市c13", "pinyin": "c13", "code": "c-13"},
        {"name": "城市c14", "pinyin": "c14", "code": "c-14"},
        {"name": "城市d0", "pinyin": "d0", "code": "d-0"},
        {"name": "城市d1", "pinyin": "d1", "code": "d-1"},
        {"name": "城市d2", "pinyin": "d2", "code": "d-2"},
        {"name": "城市d3", "pinyin": "d3", "code": "d-3"},
        {"name": "城市d4", "pinyin": "d4", "code": "d-4"},
        {"name": "城市d5", "pinyin": "d5", "code": "d-5"},
        {"name": "城市d6", "pinyin": "d6", "code": "d-6"},
        {"name": "城市d7", "pinyin": "d7", "code": "d-7"},
        {"name": "城市d8", "pinyin": "d8", "code": "d-8"},
        {"name": "城市d9", "pinyin": "d9", "code": "d-9"},
        {"name": "城市d10", "pinyin": "d10", "code": "d-10"},
        {"name": "城市d11", "pinyin": "d11", "code": "d-11"},
        {"name": "城市d12", "pinyin": "d12", "code": "d-12"},
        {"name": "城市d13", "pinyin": "d13", "code": "d-13"},
        {"name": "城市d14", "pinyin": "d14", "code": "d-14"},
        {"name": "城市e0", "pinyin": "e0", "code": "e-0"},
        {"name": "城市e1", "pinyin": "e1", "code": "e-1"},
        {"name": "城市e2", "pinyin": "e2", "code": "e-2"},
        {"name": "城市e3", "pinyin": "e3", "code": "e-3"},
        {"name": "城市e4", "pinyin": "e4", "code": "e-4"},
        {"name": "城市e5", "pinyin": "e5", "code": "e-5"},
        {"name": "城市e6", "pinyin": "e6", "code": "e-6"},
        {"name": "城市e7", "pinyin": "e7", "code": "e-7"},
        {"name": "城市e8", "pinyin": "e8", "code": "e-8"},
        {"name": "城市e9", "pinyin": "e9", "code": "e-9"},
        {"name": "城市e10", "pinyin": "e10", "code": "e-10"},
        {"name": "城市e11", "pinyin": "e11", "code": "e-11"},
        {"name": "城市e12", "pinyin": "e12", "code": "e-12"},
        {"name": "城市e13", "pinyin": "e13", "code": "e-13"},
        {"name": "城市e14", "pinyin": "e14", "code": "e-14"},
        {"name": "城市f0", "pinyin": "f0", "code": "f-0"},
        {"name": "城市f1", "pinyin": "f1", "code": "f-1"},
        {"name": "城市f2", "pinyin": "f2", "code": "f-2"},
        {"name": "城市f3", "pinyin": "f3", "code": "f-3"},
        {"name": "城市f4", "pinyin": "f4", "code": "f-4"},
        {"name": "城市f5", "pinyin": "f5", "code": "f-5"},
        {"name": "城市f6", "pinyin": "f6", "code": "f-6"},
        {"name": "城市f7", "pinyin": "f7", "code": "f-7"},
        {"name": "城市f8", "pinyin": "f8", "code": "f-8"},
        {"name": "城市f9", "pinyin": "f9", "code": "f-9"},
        {"name": "城市f10", "pinyin": "f10", "code": "f-10"},
        {"name": "城市f11", "pinyin": "f11", "code": "f-11"},
        {"name": "城市f12", "pinyin": "f12", "code": "f-12"},
        {"name": "城市f13", "pinyin": "f13", "code": "f-13"},
        {"name": "城市f14", "pinyin": "f14", "code": "f-14"},
        {"name": "城市g0", "pinyin": "g0", "code": "g-0"},
        {"name": "城市g1", "pinyin": "g1", "code": "g-1"},
        {"name": "城市g2", "pinyin": "g2", "code": "g-2"},
        {"name": "城市g3", "pinyin": "g3", "code": "g-3"},
        {"name": "城市g4", "pinyin": "g4", "code": "g-4"},
        {"name": "城市g5", "pinyin": "g5", "code": "g-5"},
        {"name": "城市g6", "pinyin": "g6", "code": "g-6"},
        {"name": "城市g7", "pinyin": "g7", "code": "g-7"},
        {"name": "城市g8", "pinyin": "g8", "code": "g-8"},
        {"name": "城市g9", "pinyin": "g9", "code": "g-9"},
        {"name": "城市g10", "pinyin": "g10", "code": "g-10"},
        {"name": "城市g11", "pinyin": "g11", "code": "g-11"},
        {"name": "城市g12", "pinyin": "g12", "code": "g-12"},
        {"name": "城市g13", "pinyin": "g13", "code": "g-13"},
        {"name": "城市g14", "pinyin": "g14", "code": "g-14"},
        {"name": "城市h0", "pinyin": "h0", "code": "h-0"},
        {"name": "城市h1", "pinyin": "h1", "code": "h-1"},
        {"name": "城市h2", "pinyin": "h2", "code": "h-2"},
        {"name": "城市h3", "pinyin": "h3", "code": "h-3"},
        {"name": "城市h4", "pinyin": "h4", "code": "h-4"},
        {"name": "城市h5", "pinyin": "h5", "code": "h-5"},
        {"name": "城市h6", "pinyin": "h6", "code": "h-6"},
        {"name": "城市h7", "pinyin": "h7", "code": "h-7"},
        {"name": "城市h8", "pinyin": "h8", "code": "h-8"},
        {"name": "城市h9", "pinyin": "h9", "code": "h-9"},
        {"name": "城市h10", "pinyin": "h10", "code": "h-10"},
        {"name": "城市h11", "pinyin": "h11", "code": "h-11"},
        {"name": "城市h12", "pinyin": "h12", "code": "h-12"},
        {"name": "城市h13", "pinyin": "h13", "code": "h-13"},
        {"name": "城市h14", "pinyin": "h14", "code": "h-14"},
        {"name": "城市i0", "pinyin": "i0", "code": "i-0"},
        {"name": "城市i1", "pinyin": "i1", "code": "i-1"},
        {"name": "城市i2", "pinyin": "i2", "code": "i-2"},
        {"name": "城市i3", "pinyin": "i3", "code": "i-3"},
        {"name": "城市i4", "pinyin": "i4", "code": "i-4"},
        {"name": "城市i5", "pinyin": "i5", "code": "i-5"},
        {"name": "城市i6", "pinyin": "i6", "code": "i-6"},
        {"name": "城市i7", "pinyin": "i7", "code": "i-7"},
        {"name": "城市i8", "pinyin": "i8", "code": "i-8"},
        {"name": "城市i9", "pinyin": "i9", "code": "i-9"},
        {"name": "城市i10", "pinyin": "i10", "code": "i-10"},
        {"name": "城市i11", "pinyin": "i11", "code": "i-11"},
        {"name": "城市i12", "pinyin": "i12", "code": "i-12"},
        {"name": "城市i13", "pinyin": "i13", "code": "i-13"},
        {"name": "城市i14", "pinyin": "i14", "code": "i-14"},
        {"name": "城市j0", "pinyin": "j0", "code": "j-0"},
        {"name": "城市j1", "pinyin": "j1", "code": "j-1"},
        {"name": "城市j2", "pinyin": "j2", "code": "j-2"},
        {"name": "城市j3", "pinyin": "j3", "code": "j-3"},
        {"name": "城市j4", "pinyin": "j4", "code": "j-4"},
        {"name": "城市j5", "pinyin": "j5", "code": "j-5"},
        {"name": "城市j6", "pinyin": "j6", "code": "j-6"},
        {"name": "城市j7", "pinyin": "j7", "code": "j-7"},
        {"name": "城市j8", "pinyin": "j8", "code": "j-8"},
        {"name": "城市j9", "pinyin": "j9", "code": "j-9"},
        {"name": "城市j10", "pinyin": "j10", "code": "j-10"},
        {"name": "城市j11", "pinyin": "j11", "code": "j-11"},
        {"name": "城市j12", "pinyin": "j12", "code": "j-12"},
        {"name": "城市j13", "pinyin": "j13", "code": "j-13"},
        {"name": "城市j14", "pinyin": "j14", "code": "j-14"},
        {"name": "城市k0", "pinyin": "k0", "code": "k-0"},
        {"name": "城市k1", "pinyin": "k1", "code": "k-1"},
        {"name": "城市k2", "pinyin": "k2", "code": "k-2"},
        {"name": "城市k3", "pinyin": "k3", "code": "k-3"},
        {"name": "城市k4", "pinyin": "k4", "code": "k-4"},
        {"name": "城市k5", "pinyin": "k5", "code": "k-5"},
        {"name": "城市k6", "pinyin": "k6", "code": "k-6"},
        {"name": "城市k7", "pinyin": "k7", "code": "k-7"},
        {"name": "城市k8", "pinyin": "k8", "code": "k-8"},
        {"name": "城市k9", "pinyin": "k9", "code": "k-9"},
        {"name": "城市k10", "pinyin": "k10", "code": "k-10"},
        {"name": "城市k11", "pinyin": "k11", "code": "k-11"},
        {"name": "城市k12", "pinyin": "k12", "code": "k-12"},
        {"name": "城市k13", "pinyin": "k13", "code": "k-13"},
        {"name": "城市k14", "pinyin": "k14", "code": "k-14"},
        {"name": "城市l0", "pinyin": "l0", "code": "l-0"},
        {"name": "城市l1", "pinyin": "l1", "code": "l-1"},
        {"name": "城市l2", "pinyin": "l2", "code": "l-2"},
        {"name": "城市l3", "pinyin": "l3", "code": "l-3"},
        {"name": "城市l4", "pinyin": "l4", "code": "l-4"},
        {"name": "城市l5", "pinyin": "l5", "code": "l-5"},
        {"name": "城市l6", "pinyin": "l6", "code": "l-6"},
        {"name": "城市l7", "pinyin": "l7", "code": "l-7"},
        {"name": "城市l8", "pinyin": "l8", "code": "l-8"},
        {"name": "城市l9", "pinyin": "l9", "code": "l-9"},
        {"name": "城市l10", "pinyin": "l10", "code": "l-10"},
        {"name": "城市l11", "pinyin": "l11", "code": "l-11"},
        {"name": "城市l12", "pinyin": "l12", "code": "l-12"},
        {"name": "城市l13", "pinyin": "l13", "code": "l-13"},
        {"name": "城市l14", "pinyin": "l14", "code": "l-14"},
        {"name": "城市m0", "pinyin": "m0", "code": "m-0"},
        {"name": "城市m1", "pinyin": "m1", "code": "m-1"},
        {"name": "城市m2", "pinyin": "m2", "code": "m-2"},
        {"name": "城市m3", "pinyin": "m3", "code": "m-3"},
        {"name": "城市m4", "pinyin": "m4", "code": "m-4"},
        {"name": "城市m5", "pinyin": "m5", "code": "m-5"},
        {"name": "城市m6", "pinyin": "m6", "code": "m-6"},
        {"name": "城市m7", "pinyin": "m7", "code": "m-7"},
        {"name": "城市m8", "pinyin": "m8", "code": "m-8"},
        {"name": "城市m9", "pinyin": "m9", "code": "m-9"},
        {"name": "城市m10", "pinyin": "m10", "code": "m-10"},
        {"name": "城市m11", "pinyin": "m11", "code": "m-11"},
        {"name": "城市m12", "pinyin": "m12", "code": "m-12"},
        {"name": "城市m13", "pinyin": "m13", "code": "m-13"},
        {"name": "城市m14", "pinyin": "m14", "code": "m-14"},
        {"name": "城市n0", "pinyin": "n0", "code": "n-0"},
        {"name": "城市n1", "pinyin": "n1", "code": "n-1"},
        {"name": "城市n2", "pinyin": "n2", "code": "n-2"},
        {"name": "城市n3", "pinyin": "n3", "code": "n-3"},
        {"name": "城市n4", "pinyin": "n4", "code": "n-4"},
        {"name": "城市n5", "pinyin": "n5", "code": "n-5"},
        {"name": "城市n6", "pinyin": "n6", "code": "n-6"},
        {"name": "城市n7", "pinyin": "n7", "code": "n-7"},
        {"name": "城市n8", "pinyin": "n8", "code": "n-8"},
        {"name": "城市n9", "pinyin": "n9", "code": "n-9"},
        {"name": "城市n10", "pinyin": "n10", "code": "n-10"},
        {"name": "城市n11", "pinyin": "n11", "code": "n-11"},
        {"name": "城市n12", "pinyin": "n12", "code": "n-12"},
        {"name": "城市n13", "pinyin": "n13", "code": "n-13"},
        {"name": "城市n14", "pinyin": "n14", "code": "n-14"},
        {"name": "城市o0", "pinyin": "o0", "code": "o-0"},
        {"name": "城市o1", "pinyin": "o1", "code": "o-1"},
        {"name": "城市o2", "pinyin": "o2", "code": "o-2"},
        {"name": "城市o3", "pinyin": "o3", "code": "o-3"},
        {"name": "城市o4", "pinyin": "o4", "code": "o-4"},
        {"name": "城市o5", "pinyin": "o5", "code": "o-5"},
        {"name": "城市o6", "pinyin": "o6", "code": "o-6"},
        {"name": "城市o7", "pinyin": "o7", "code": "o-7"},
        {"name": "城市o8", "pinyin": "o8", "code": "o-8"},
        {"name": "城市o9", "pinyin": "o9", "code": "o-9"},
        {"name": "城市o10", "pinyin": "o10", "code": "o-10"},
        {"name": "城市o11", "pinyin": "o11", "code": "o-11"},
        {"name": "城市o12", "pinyin": "o12", "code": "o-12"},
        {"name": "城市o13", "pinyin": "o13", "code": "o-13"},
        {"name": "城市o14", "pinyin": "o14", "code": "o-14"},
        {"name": "城市p0", "pinyin": "p0", "code": "p-0"},
        {"name": "城市p1", "pinyin": "p1", "code": "p-1"},
        {"name": "城市p2", "pinyin": "p2", "code": "p-2"},
        {"name": "城市p3", "pinyin": "p3", "code": "p-3"},
        {"name": "城市p4", "pinyin": "p4", "code": "p-4"},
        {"name": "城市p5", "pinyin": "p5", "code": "p-5"},
        {"name": "城市p6", "pinyin": "p6", "code": "p-6"},
        {"name": "城市p7", "pinyin": "p7", "code": "p-7"},
        {"name": "城市p8", "pinyin": "p8", "code": "p-8"},
        {"name": "城市p9", "pinyin": "p9", "code": "p-9"},
        {"name": "城市p10", "pinyin": "p10", "code": "p-10"},
        {"name": "城市p11", "pinyin": "p11", "code": "p-11"},
        {"name": "城市p12", "pinyin": "p12", "code": "p-12"},
        {"name": "城市p13", "pinyin": "p13", "code": "p-13"},
        {"name": "城市p14", "pinyin": "p14", "code": "p-14"},
        {"name": "城市q0", "pinyin": "q0", "code": "q-0"},
        {"name": "城市q1", "pinyin": "q1", "code": "q-1"},
        {"name": "城市q2", "pinyin": "q2", "code": "q-2"},
        {"name": "城市q3", "pinyin": "q3", "code": "q-3"},
        {"name": "城市q4", "pinyin": "q4", "code": "q-4"},
        {"name": "城市q5", "pinyin": "q5", "code": "q-5"},
        {"name": "城市q6", "pinyin": "q6", "code": "q-6"},
        {"name": "城市q7", "pinyin": "q7", "code": "q-7"},
        {"name": "城市q8", "pinyin": "q8", "code": "q-8"},
        {"name": "城市q9", "pinyin": "q9", "code": "q-9"},
        {"name": "城市q10", "pinyin": "q10", "code": "q-10"},
        {"name": "城市q11", "pinyin": "q11", "code": "q-11"},
        {"name": "城市q12", "pinyin": "q12", "code": "q-12"},
        {"name": "城市q13", "pinyin": "q13", "code": "q-13"},
        {"name": "城市q14", "pinyin": "q14", "code": "q-14"},
        {"name": "城市r0", "pinyin": "r0", "code": "r-0"},
        {"name": "城市r1", "pinyin": "r1", "code": "r-1"},
        {"name": "城市r2", "pinyin": "r2", "code": "r-2"},
        {"name": "城市r3", "pinyin": "r3", "code": "r-3"},
        {"name": "城市r4", "pinyin": "r4", "code": "r-4"},
        {"name": "城市r5", "pinyin": "r5", "code": "r-5"},
        {"name": "城市r6", "pinyin": "r6", "code": "r-6"},
        {"name": "城市r7", "pinyin": "r7", "code": "r-7"},
        {"name": "城市r8", "pinyin": "r8", "code": "r-8"},
        {"name": "城市r9", "pinyin": "r9", "code": "r-9"},
        {"name": "城市r10", "pinyin": "r10", "code": "r-10"},
        {"name": "城市r11", "pinyin": "r11", "code": "r-11"},
        {"name": "城市r12", "pinyin": "r12", "code": "r-12"},
        {"name": "城市r13", "pinyin": "r13", "code": "r-13"},
        {"name": "城市r14", "pinyin": "r14", "code": "r-14"},
        {"name": "城市s0", "pinyin": "s0", "code": "s-0"},
        {"name": "城市s1", "pinyin": "s1", "code": "s-1"},
        {"name": "城市s2", "pinyin": "s2", "code": "s-2"},
        {"name": "城市s3", "pinyin": "s3", "code": "s-3"},
        {"name": "城市s4", "pinyin": "s4", "code": "s-4"},
        {"name": "城市s5", "pinyin": "s5", "code": "s-5"},
        {"name": "城市s6", "pinyin": "s6", "code": "s-6"},
        {"name": "城市s7", "pinyin": "s7", "code": "s-7"},
        {"name": "城市s8", "pinyin": "s8", "code": "s-8"},
        {"name": "城市s9", "pinyin": "s9", "code": "s-9"},
        {"name": "城市s10", "pinyin": "s10", "code": "s-10"},
        {"name": "城市s11", "pinyin": "s11", "code": "s-11"},
        {"name": "城市s12", "pinyin": "s12", "code": "s-12"},
        {"name": "城市s13", "pinyin": "s13", "code": "s-13"},
        {"name": "城市s14", "pinyin": "s14", "code": "s-14"},
        {"name": "城市t0", "pinyin": "t0", "code": "t-0"},
        {"name": "城市t1", "pinyin": "t1", "code": "t-1"},
        {"name": "城市t2", "pinyin": "t2", "code": "t-2"},
        {"name": "城市t3", "pinyin": "t3", "code": "t-3"},
        {"name": "城市t4", "pinyin": "t4", "code": "t-4"},
        {"name": "城市t5", "pinyin": "t5", "code": "t-5"},
        {"name": "城市t6", "pinyin": "t6", "code": "t-6"},
        {"name": "城市t7", "pinyin": "t7", "code": "t-7"},
        {"name": "城市t8", "pinyin": "t8", "code": "t-8"},
        {"name": "城市t9", "pinyin": "t9", "code": "t-9"},
        {"name": "城市t10", "pinyin": "t10", "code": "t-10"},
        {"name": "城市t11", "pinyin": "t11", "code": "t-11"},
        {"name": "城市t12", "pinyin": "t12", "code": "t-12"},
        {"name": "城市t13", "pinyin": "t13", "code": "t-13"},
        {"name": "城市t14", "pinyin": "t14", "code": "t-14"},
        {"name": "城市u0", "pinyin": "u0", "code": "u-0"},
        {"name": "城市u1", "pinyin": "u1", "code": "u-1"},
        {"name": "城市u2", "pinyin": "u2", "code": "u-2"},
        {"name": "城市u3", "pinyin": "u3", "code": "u-3"},
        {"name": "城市u4", "pinyin": "u4", "code": "u-4"},
        {"name": "城市u5", "pinyin": "u5", "code": "u-5"},
        {"name": "城市u6", "pinyin": "u6", "code": "u-6"},
        {"name": "城市u7", "pinyin": "u7", "code": "u-7"},
        {"name": "城市u8", "pinyin": "u8", "code": "u-8"},
        {"name": "城市u9", "pinyin": "u9", "code": "u-9"},
        {"name": "城市u10", "pinyin": "u10", "code": "u-10"},
        {"name": "城市u11", "pinyin": "u11", "code": "u-11"},
        {"name": "城市u12", "pinyin": "u12", "code": "u-12"},
        {"name": "城市u13", "pinyin": "u13", "code": "u-13"},
        {"name": "城市u14", "pinyin": "u14", "code": "u-14"},
        {"name": "城市v0", "pinyin": "v0", "code": "v-0"},
        {"name": "城市v1", "pinyin": "v1", "code": "v-1"},
        {"name": "城市v2", "pinyin": "v2", "code": "v-2"},
        {"name": "城市v3", "pinyin": "v3", "code": "v-3"},
        {"name": "城市v4", "pinyin": "v4", "code": "v-4"},
        {"name": "城市v5", "pinyin": "v5", "code": "v-5"},
        {"name": "城市v6", "pinyin": "v6", "code": "v-6"},
        {"name": "城市v7", "pinyin": "v7", "code": "v-7"},
        {"name": "城市v8", "pinyin": "v8", "code": "v-8"},
        {"name": "城市v9", "pinyin": "v9", "code": "v-9"},
        {"name": "城市v10", "pinyin": "v10", "code": "v-10"},
        {"name": "城市v11", "pinyin": "v11", "code": "v-11"},
        {"name": "城市v12", "pinyin": "v12", "code": "v-12"},
        {"name": "城市v13", "pinyin": "v13", "code": "v-13"},
        {"name": "城市v14", "pinyin": "v14", "code": "v-14"},
        {"name": "城市w0", "pinyin": "w0", "code": "w-0"},
        {"name": "城市w1", "pinyin": "w1", "code": "w-1"},
        {"name": "城市w2", "pinyin": "w2", "code": "w-2"},
        {"name": "城市w3", "pinyin": "w3", "code": "w-3"},
        {"name": "城市w4", "pinyin": "w4", "code": "w-4"},
        {"name": "城市w5", "pinyin": "w5", "code": "w-5"},
        {"name": "城市w6", "pinyin": "w6", "code": "w-6"},
        {"name": "城市w7", "pinyin": "w7", "code": "w-7"},
        {"name": "城市w8", "pinyin": "w8", "code": "w-8"},
        {"name": "城市w9", "pinyin": "w9", "code": "w-9"},
        {"name": "城市w10", "pinyin": "w10", "code": "w-10"},
        {"name": "城市w11", "pinyin": "w11", "code": "w-11"},
        {"name": "城市w12", "pinyin": "w12", "code": "w-12"},
        {"name": "城市w13", "pinyin": "w13", "code": "w-13"},
        {"name": "城市w14", "pinyin": "w14", "code": "w-14"},
        {"name": "城市x0", "pinyin": "x0", "code": "x-0"},
        {"name": "城市x1", "pinyin": "x1", "code": "x-1"},
        {"name": "城市x2", "pinyin": "x2", "code": "x-2"},
        {"name": "城市x3", "pinyin": "x3", "code": "x-3"},
        {"name": "城市x4", "pinyin": "x4", "code": "x-4"},
        {"name": "城市x5", "pinyin": "x5", "code": "x-5"},
        {"name": "城市x6", "pinyin": "x6", "code": "x-6"},
        {"name": "城市x7", "pinyin": "x7", "code": "x-7"},
        {"name": "城市x8", "pinyin": "x8", "code": "x-8"},
        {"name": "城市x9", "pinyin": "x9", "code": "x-9"},
        {"name": "城市x10", "pinyin": "x10", "code": "x-10"},
        {"name": "城市x11", "pinyin": "x11", "code": "x-11"},
        {"name": "城市x12", "pinyin": "x12", "code": "x-12"},
        {"name": "城市x13", "pinyin": "x13", "code": "x-13"},
        {"name": "城市x14", "pinyin": "x14", "code": "x-14"},
        {"name": "城市y0", "pinyin": "y0", "code": "y-0"},
        {"name": "城市y1", "pinyin": "y1", "code": "y-1"},
        {"name": "城市y2", "pinyin": "y2", "code": "y-2"},
        {"name": "城市y3", "pinyin": "y3", "code": "y-3"},
        {"name": "城市y4", "pinyin": "y4", "code": "y-4"},
        {"name": "城市y5", "pinyin": "y5", "code": "y-5"},
        {"name": "城市y6", "pinyin": "y6", "code": "y-6"},
        {"name": "城市y7", "pinyin": "y7", "code": "y-7"},
        {"name": "城市y8", "pinyin": "y8", "code": "y-8"},
        {"name": "城市y9", "pinyin": "y9", "code": "y-9"},
        {"name": "城市y10", "pinyin": "y10", "code": "y-10"},
        {"name": "城市y11", "pinyin": "y11", "code": "y-11"},
        {"name": "城市y12", "pinyin": "y12", "code": "y-12"},
        {"name": "城市y13", "pinyin": "y13", "code": "y-13"},
        {"name": "城市y14", "pinyin": "y14", "code": "y-14"},
        {"name": "城市z0", "pinyin": "z0", "code": "z-0"},
        {"name": "城市z1", "pinyin": "z1", "code": "z-1"},
        {"name": "城市z2", "pinyin": "z2", "code": "z-2"},
        {"name": "城市z3", "pinyin": "z3", "code": "z-3"},
        {"name": "城市z4", "pinyin": "z4", "code": "z-4"},
        {"name": "城市z5", "pinyin": "z5", "code": "z-5"},
        {"name": "城市z6", "pinyin": "z6", "code": "z-6"},
        {"name": "城市z7", "pinyin": "z7", "code": "z-7"},
        {"name": "城市z8", "pinyin": "z8", "code": "z-8"},
        {"name": "城市z9", "pinyin": "z9", "code": "z-9"},
        {"name": "城市z10", "pinyin": "z10", "code": "z-10"},
        {"name": "城市z11", "pinyin": "z11", "code": "z-11"},
        {"name": "城市z12", "pinyin": "z12", "code": "z-12"},
        {"name": "城市z13", "pinyin": "z13", "code": "z-13"},
        {"name": "城市z14", "pinyin": "z14", "code": "z-14"}
    ]
}
